package com.mingotuo.chainofresponsibility;

import com.mingotuo.chainofresponsibility.ape.AndroidApe;
import com.mingotuo.chainofresponsibility.ape.ProgramApe;
import com.mingotuo.chainofresponsibility.leader.LeaderHelper;

public class Client {

  public static void main(String[] args) {
    ProgramApe ape = new AndroidApe((int) (Math.random() * 30000));
    LeaderHelper helper = LeaderHelper.getInstance();
    helper.handleRequest(ape);
  }
}
