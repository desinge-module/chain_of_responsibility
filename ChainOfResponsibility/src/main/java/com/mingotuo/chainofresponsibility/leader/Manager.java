package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

/**
 * 部门经理类
 */
public class Manager extends Leader {

  public Manager() {
    super(10000);
  }

  @Override public void reply(ProgramApe ape) {
    System.out.println(ape.getExpenses());
    System.out.println(ape.getApply());
    System.out.println("Manager: Of course Yes!");
  }
}
