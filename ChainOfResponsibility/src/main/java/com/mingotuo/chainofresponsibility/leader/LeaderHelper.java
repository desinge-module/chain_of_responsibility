package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

public class LeaderHelper {
  private static LeaderHelper INSTANCE = Holder.getHolder();

  public static LeaderHelper getInstance() {
    return INSTANCE;
  }

  private GroupLeader mGroupLeader;
  private Manager mManager;
  private Director mDirector;
  private Boss mBoss;

  private LeaderHelper() {
    this.mGroupLeader = new GroupLeader();
    this.mManager = new Manager();
    this.mDirector = new Director();
    this.mBoss = new Boss();

    this.mGroupLeader.setSuperiorLeader(mDirector);
    this.mDirector.setSuperiorLeader(mManager);
    this.mManager.setSuperiorLeader(mBoss);
  }

  public void handleRequest(ProgramApe ape) {
    this.mGroupLeader.handleRequest(ape);
  }

  private static final class Holder {
    private static LeaderHelper getHolder() {
      return new LeaderHelper();
    }
  }
}
