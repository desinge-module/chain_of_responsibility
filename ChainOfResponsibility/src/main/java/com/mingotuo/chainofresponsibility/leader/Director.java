package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

/**
 * 项目主管类
 */
public class Director extends Leader {

  public Director() {
    super(5000);
  }

  @Override public void reply(ProgramApe ape) {
    System.out.println(ape.getExpenses());
    System.out.println(ape.getApply());
    System.out.println("Director: Of course Yes!");
  }
}
