package com.mingotuo.injectview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import com.mingotuo.injectview.injection.InjectView;
import com.mingotuo.injectview.injection.InjectViewParse;

public class MainActivity extends AppCompatActivity {

  @InjectView(id = R.id.hello)
  private TextView mTextView;
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    InjectViewParse.Inject(this);
    mTextView.setText("你好");
  }
}
