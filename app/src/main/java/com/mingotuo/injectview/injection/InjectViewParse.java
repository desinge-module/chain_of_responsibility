package com.mingotuo.injectview.injection;

import android.app.Activity;
import android.view.View;
import java.lang.reflect.Field;

public class InjectViewParse {

  public static void Inject(Object object) {
    try {
      parse(object);
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  private static void parse(Object object) throws Throwable {
    final Class<?> clazz = object.getClass();
    View view = null;
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(InjectView.class)) {
        InjectView injectView = field.getAnnotation(InjectView.class);
        int id = injectView.id();
        if (id < 0) {
          throw new Exception("id must be not null");
        } else {
          field.setAccessible(true);
          if (object instanceof View) {
            view = ((View) object).findViewById(id);
          } else if (object instanceof Activity) {
            view = ((Activity) object).findViewById(id);
          }
          field.set(object, view);
        }
      }
    }
  }
}
